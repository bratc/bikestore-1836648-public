import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { AccessDeniedComponent } from './auth/access-denied/access-denied.component';
import { Authority } from './auth/auth-shared/constants/authority.constants';
import { UserRouteAccessService } from './auth/user-route-access.service';
import { ListUserComponent } from './components/list-user/list-user.component';


const routes: Routes = [
  {
    path: 'dashboard',
    data: {
      authorities: [Authority.ADMIN]
    },
    canActivate: [UserRouteAccessService],
    loadChildren: () => import('./dashboard/dashboard.module')
    .then(m => m.DashboardModule)
  },
  {
    path: 'list-user',
    component: ListUserComponent
  },
  {
    path: 'accessdenied',
    component: AccessDeniedComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    redirectTo: 'list-user',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
