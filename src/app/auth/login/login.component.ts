import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ICredentials, Credentials } from '../auth-shared/models/credentials.model';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  loginForm = this.fb.group({
    username: [''],
    password: ['']
  });
  constructor(
    private fb: FormBuilder,
    private loginService: LoginService
    ) { }

  ngOnInit(): void {
  }

  login(): void {
    console.warn(' DATOS ', this.loginForm.value);
    const credentials: ICredentials = new Credentials();

    credentials.username = this.loginForm.value.username;
    credentials.password = this.loginForm.value.password;
    credentials.rememberMe = this.loginForm.value.rememberMe;


    this.loginService.login(credentials)
    .subscribe((res: any) => {
      console.warn('DATA LOGIN CONTROLLER - Ok Login ', res);
    },
    (error: any) => {
      if (error.status === 400) {
        console.warn('Usuario o contraseña incorrectos');
      }
    });


  }
}
