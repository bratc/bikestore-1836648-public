export interface ICredentials {
    login?: string;
    username?: string;
    password?: string;
    roles?: string[];
    rememberMe?: boolean;
}

export class Credentials implements ICredentials {
    constructor(
        login?: string,
        public username?: string,
        public password?: string,
        public roles?: string[],
        public rememberMe?: boolean
    ){}
}
