import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-count-user',
  templateUrl: './count-user.component.html',
  styleUrls: ['./count-user.component.sass']
})
export class CountUserComponent implements OnInit {

  selectedItem  = 'T';

  @Input() countUserMale: number;
  @Input() countUserFemale: number;

  @Output() typeOptionEvent: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
 
  }

  itemRadioSelected(): any {
    this.typeOptionEvent.emit(this.selectedItem);
  }
}
