import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BikesService } from '../bikes.service';
import { IBike } from '../bike.interface';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-bikes-update',
  templateUrl: './bikes-update.component.html',
  styleUrls: ['./bikes-update.component.sass']
})
export class BikesUpdateComponent implements OnInit {
  bike: IBike;
  bikeFormUpdateGroup: FormGroup;
  constructor(
    private activatedRoute: ActivatedRoute,
    private bikesService: BikesService,
    private formBuilder: FormBuilder,
    private router: Router
    ) {

      this.bikeFormUpdateGroup = this.formBuilder.group({
        id: [],
        model: ['', [
                      Validators.required,
                      Validators.minLength(2),
                      Validators.maxLength(5)
                    ]],
        price: ['', Validators.required],
        serial: ['']
      });


     }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    console.warn('ID GET', id);
    this.bikesService.getBikeById(id)
    .subscribe(res => {
      console.warn('DATA UPDATE',res);
      this.bike = res;
      this.loadForm(this.bike);
    });

   
  }

  updateBike() {
    this.bikesService.updateBike(this.bikeFormUpdateGroup.value)
    .subscribe(res => {
      console.warn('Actualizado...');
      this.router.navigate(['/bikes/bikes-list']); // Redireccionar
    }, err => console.warn(err));
  }

  private loadForm(bike: IBike) {
      this.bikeFormUpdateGroup.patchValue({
        id: bike.id,
        model: bike.model,
        price: bike.price,
        serial: bike.serial
      });
  }

}
