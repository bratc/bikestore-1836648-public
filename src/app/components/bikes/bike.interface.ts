export interface IBike {
    id?: number;
    model: string;
    price: number;
    serial: string;
    status: boolean;
}
