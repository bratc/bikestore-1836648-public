import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.sass']
})
export class ListUserComponent implements OnInit {
  listUsers = [
    {
      nombres: "Juan Mario",
      edad: 20,
      genero: 'M', 
      dependencia: 'SISTEMAS'
    },
    {
      nombres: "Andres Bermudez",
      edad: 28,
      genero: 'M', 
      dependencia: 'CONTABILIDAD'
    },
    {
      nombres: "Sandra Alvear",
      edad: 26,
      genero: 'F', 
      dependencia: 'SISTEMAS'
    },
    {
      nombres: "Jaime Mesa",
      edad: 28,
      genero: 'M', 
      dependencia: 'SISTEMAS'
    },
    {
      nombres: "Carolina Quintero",
      edad: 28,
      genero: 'F', 
      dependencia: 'ADMINISTRACION'
    },
    {
      nombres: "Sandra Patricia",
      edad: 27,
      genero: 'F', 
      dependencia: 'SISTEMAS'
    },
    {
      nombres: "Mauricio Aguilar",
      edad: 21,
      genero: 'M', 
      dependencia: 'SISTEMAS'
    },
  ];

  getSelectedType = 'T';

  constructor() { }

  ngOnInit(): void {
  }

  sendCountFemale(): number {
    return this.listUsers.filter(item => item.genero === 'F').length;
  }

  sendCountMale(): number {
    return this.listUsers.filter(item => item.genero === 'M').length;
  }

  getDataChild(value: string): void {
    console.warn('Get Data ', value);
    this.getSelectedType = value;
  }

}
